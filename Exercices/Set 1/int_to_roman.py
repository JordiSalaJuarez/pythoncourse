def int_to_roman(n):
    """Conversion from integer n to roman number as a string
    >>> int_to_roman(9)
    'IX'
    >>> int_to_roman(666)
    'DCLXVI'
    >>> int_to_roman(2020)
    'MMXX'
    >>> list(map(int_to_roman,[1,5,10,50,100,500,1000]))
    ['I','V','X','L','C','D','M']
    """