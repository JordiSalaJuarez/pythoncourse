from odd import odd
def even(n):
    """Tells whether n if en is even
    You must call odd function
    >>> odd(6)
    True
    >>> odd(13)
    False
    >>> odd(-1)
    Flase
    >>> odd(-4)
    True
    """
