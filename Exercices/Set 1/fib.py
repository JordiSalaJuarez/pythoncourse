def fib(n):
    """ returns fibonacci of n
    >>> fib(1)
    1
    >>> fib(6)
    8
    >>> fib(20)
    6765
    """