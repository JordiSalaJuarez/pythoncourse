def max_key_value(d):
    """ List of maximums for each key and value
    >>> max_key_value({1:0, 2:3, 5:2, 3:7})
    [1, 3, 5, 7]
    """