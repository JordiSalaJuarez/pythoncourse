def double(n):
    """Returns de double of n
    >>> double(2)
    4
    >>> double(32)
    64
    >>> double("py")
    "pypy"
    >>> double([1,2,3])
    [[1,2,3],[1,2,3]]
    """