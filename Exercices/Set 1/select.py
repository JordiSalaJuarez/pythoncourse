def select(l,k):
    """Takes the k-th smallest element from l
    >>> select([3,2,1], 0)
    1
    >>> select([1,2,3,4,5], 2)
    3
    >>> select(list(range(10)),10)
    10
    """