def odd(n):
    """Tells wether n is odd or not
    >>> odd(6)
    False
    >>> odd(13)
    True
    >>> odd(-1)
    True
    >>> odd(-4)
    False
    """