# Python Course
## Introduction
- Running first test **Hello World!**
- Variable assignment
- tuples, lists and strings
- matrices, indexing and binaryOp with iterables
- conditionals, loops, conditional assignments
- list/expression comprehensions
- standard functions type, min, max, sum, in , is
- first order functions map, filter, reduce 

## Problems
- def add(x,y)
- def add(*args)
- def max(x,y)
- def max(*args)
- def min(*args) (just using max)
- def odd(value) (tell its odd)
- def even(value) <- not odd(value)
- def 

- def end(iterable)
- def begin(iterable)
- def tail(iterable)
- def head(iterable)
- def reverse(iterable) iterative
- def reverse recursive (with just tail and head)
- def map_list_comp (map function as list comprehension)
- def filter_list_comp (filter function as list comprehension)

- def map_yield (map function with yield)
- def map_reduce (map function just using reduce)
- def filter (filter function as list comprehension)
- def my_filter (filter function with yield)

- def partition(iterable, v) return values < v , values == v ,values > v  (suppose this is O(n))
- def median(iterable) (using partition) O(n)
- def flatten

## Debugging
- gdb
- prints
- taking more information from variables (dir)
